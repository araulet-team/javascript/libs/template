'use strict'

const hello = require('../lib/index.js')

describe('#index', () => {
  describe('#success', () => {
    it('should display hello world', () => {
      const result = hello('world')
      expect(result).toEqual('hello world')
    })
  })
})
