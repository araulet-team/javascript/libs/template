# template [![pipeline status](https://gitlab.com/araulet-team/javascript/libs/template/badges/master/pipeline.svg)](https://gitlab.com/araulet-team/javascript/libs/template/commits/master) [![coverage report](https://gitlab.com/araulet-team/javascript/libs/template/badges/master/coverage.svg)](https://gitlab.com/araulet-team/javascript/libs/template/commits/master) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

Libs template

Just `fork` it and start writting your module.

## Installation

```shell
$ npm install --save template
```

## Usage

* `npm test`
* `npm run lint`: standard
* `npm run lint:fix`: standard

## Authors

* **Arnaud Raulet**

## License

MIT License

Copyright (c) 2018 Arnaud Raulet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
